# Planar Scanline-Homography

MATLAB code for the article [*Scanline Homographies for Rolling-Shutter Plane Absolute Pose*](https://openaccess.thecvf.com/content/CVPR2022/html/Bai_Scanline_Homographies_for_Rolling-Shutter_Plane_Absolute_Pose_CVPR_2022_paper.html)

Authors: Fang Bai (fang.bai@yahoo.com), Agniva Sengupta (i.agniva@gmail.com), Adrien Bartoli (adrien.bartoli@gmail.com)

Accepted at: IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR) 2022


---

## Reference

```
@InProceedings{Bai_2022_CVPR,
    author    = {Bai, Fang and Sengupta, Agniva and Bartoli, Adrien},
    title     = {Scanline Homographies for Rolling-Shutter Plane Absolute Pose},
    booktitle = {Proceedings of the IEEE/CVF Conference on Computer Vision and Pattern Recognition (CVPR)},
    month     = {June},
    year      = {2022},
    pages     = {8993-9002}
}
```

---

## Contributions

All the authors have been indicated as equal contributors to this work.

Adrien Bartoli originates the idea of scanline-homography along with its estimation by B-Spline and polynomial paramterizations.
Fang Bai develops the theory of fundamental homography equation, the QR form to parameterize plane-homographies, and the alternation method for pose estimation.
Agniva Sengupta makes substantial contributions on the experiment part in the initial submission.
The article is written by Fang Bai.

---

## License

The code is relased under the [Apache License](./LICENSE).

